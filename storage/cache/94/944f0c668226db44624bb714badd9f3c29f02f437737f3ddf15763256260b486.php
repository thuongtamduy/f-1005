<?php

/* extension/module/shopping_cart_rest_api.twig */
class __TwigTemplate_183e03a676ee002f7013b27ee9c29b75fd57a7bf9444c3d8eadb4f64ee044278 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\"
                        class=\"save-changes btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"";
        // line 8
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i
                            class=\"fa fa-reply\"></i></a></div>
            <h1>";
        // line 10
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 13
            echo "                    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 19
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 20
            echo "            <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 24
        echo "        ";
        if ((isset($context["install_success"]) ? $context["install_success"] : null)) {
            // line 25
            echo "            <div class=\"alert alert-success\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["install_success"]) ? $context["install_success"] : null);
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        // line 29
        echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 31
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
                ";
        // line 34
        if (twig_test_empty((isset($context["module_shopping_cart_rest_api_licensed_on"]) ? $context["module_shopping_cart_rest_api_licensed_on"] : null))) {
            // line 35
            echo "                    ";
            echo (isset($context["xcDLOMddkCV"]) ? $context["xcDLOMddkCV"] : null);
            echo "
                ";
        }
        // line 37
        echo "                <form action=\"";
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\"
                      class=\"form-horizontal\">
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 40
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
                        <div class=\"col-sm-10\">
                            <select name=\"module_shopping_cart_rest_api_status\" id=\"input-status\" class=\"form-control restapi_status\">
                                ";
        // line 43
        if ((isset($context["module_shopping_cart_rest_api_status"]) ? $context["module_shopping_cart_rest_api_status"] : null)) {
            // line 44
            echo "                                    <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                                    <option value=\"0\">";
            // line 45
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                                ";
        } else {
            // line 47
            echo "                                    <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                                    <option value=\"0\" selected=\"selected\">";
            // line 48
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                                ";
        }
        // line 50
        echo "                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-entry-allowed-ip\">
                    <span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 55
        echo (isset($context["text_allowed_ip"]) ? $context["text_allowed_ip"] : null);
        echo "\">
                      ";
        // line 56
        echo (isset($context["entry_allowed_ip"]) ? $context["entry_allowed_ip"] : null);
        echo "
                    </span>
                        </label>
                        <div class=\"col-sm-10\">
                            <input class=\"form-control\" type=\"text\" name=\"module_shopping_cart_rest_api_allowed_ip\"
                                   value=\"";
        // line 61
        echo (isset($context["module_shopping_cart_rest_api_allowed_ip"]) ? $context["module_shopping_cart_rest_api_allowed_ip"] : null);
        echo "\"/>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-entry-key\">
              <span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 66
        echo (isset($context["text_secret_key"]) ? $context["text_secret_key"] : null);
        echo "\">
                ";
        // line 67
        echo (isset($context["entry_key"]) ? $context["entry_key"] : null);
        echo "
              </span>
                        </label>
                        <div class=\"col-sm-10\">
                            <input class=\"form-control\" id=\"input-key\" type=\"text\" name=\"module_shopping_cart_rest_api_key\"
                                   value=\"";
        // line 72
        echo (isset($context["module_shopping_cart_rest_api_key"]) ? $context["module_shopping_cart_rest_api_key"] : null);
        echo "\" required=\"required\"/>
                            <br>
                            <button type=\"button\" id=\"button-generate\" class=\"btn btn-primary\"><i class=\"fa fa-refresh\"></i> ";
        // line 74
        echo (isset($context["button_generate_api_key"]) ? $context["button_generate_api_key"] : null);
        echo "</button>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\" for=\"input-entry-order-id\">
                  <span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 79
        echo (isset($context["text_order_id"]) ? $context["text_order_id"] : null);
        echo "\">
                    ";
        // line 80
        echo (isset($context["entry_order_id"]) ? $context["entry_order_id"] : null);
        echo "
                  </span>
                        </label>
                        <div class=\"col-sm-10\">
                            ";
        // line 84
        if (twig_test_empty((isset($context["module_shopping_cart_rest_api_licensed_on"]) ? $context["module_shopping_cart_rest_api_licensed_on"] : null))) {
            // line 85
            echo "                                <div class=\"restAPILicenseError\"></div>
                                <div class=\"restAPILicenseSuccess\"></div>
                                <div class=\"restAPILicenseInfo\"></div>
                                <table class=\"table\">
                                    <tr>
                                        <td colspan=\"2\" style=\"border: none;\">
                                            <input type=\"text\" class=\"rest_api_ord_id form-control\" placeholder=\"REST-XXX-XXX\" name=\"module_shopping_cart_rest_api_order_id\" id=\"module_shopping_cart_rest_api_order_id\" value=\"";
            // line 91
            echo (isset($context["module_shopping_cart_rest_api_order_id"]) ? $context["module_shopping_cart_rest_api_order_id"] : null);
            echo "\" required=\"required\" />
                                            <br>
                                            <button type=\"button\" class=\"btn btn-success activateLicenseButton\"><i class=\"icon-ok\"></i> Activate License</button>
                                        </td>
                                    </tr>
                                </table>
                                <script type=\"text/javascript\">
                                    var domain='";
            // line 98
            echo (isset($context["hostname"]) ? $context["hostname"] : null);
            echo "';
                                    var timenow=";
            // line 99
            echo twig_date_format_filter($this->env, "now", "U");
            echo ";
                                </script>
                                <script type=\"text/javascript\" src=\"//license.opencart-api.com/validate.js?v=";
            // line 101
            echo twig_date_format_filter($this->env, "now", "U");
            echo "\"></script>
                            ";
        }
        // line 103
        echo "
                            ";
        // line 104
        if ( !twig_test_empty((isset($context["module_shopping_cart_rest_api_licensed_on"]) ? $context["module_shopping_cart_rest_api_licensed_on"] : null))) {
            // line 105
            echo "                                <input type=\"hidden\" class=\"rest_api_ord_id form-control\" name=\"module_shopping_cart_rest_api_order_id\" id=\"module_shopping_cart_rest_api_order_id\" value=\"";
            echo (isset($context["module_shopping_cart_rest_api_order_id"]) ? $context["module_shopping_cart_rest_api_order_id"] : null);
            echo "\" required=\"required\" />
                                <input name=\"nJvNVJoMHcQVIuHk\" type=\"hidden\" value=\"";
            // line 106
            echo (isset($context["module_shopping_cart_rest_api_licensed_on"]) ? $context["module_shopping_cart_rest_api_licensed_on"] : null);
            echo "\" />
                                <table class=\"table licensedTable\">
                                    <tr>
                                        <td style=\"border:none;\"><b>";
            // line 109
            echo (isset($context["module_shopping_cart_rest_api_order_id"]) ? $context["module_shopping_cart_rest_api_order_id"] : null);
            echo "</b>
                                            <span style=\"text-align:center;background-color:#c0f7a5;display: inline-block;padding: 5px 10px;\">VALID LICENSE</span>
                                        </td>
                                    </tr>
                                </table>
                            ";
        }
        // line 115
        echo "                        </div>
                    </div>
                </form>
                <div class=\"alert alert-info\">
                    <h4><i class=\"fa fa-info\"></i> Info</h4>
                    <p>Please follow the instructions in install.txt to install REST API extension.</p>
                    <p>If you need help please check out our <a
                                href=\"https://opencart-api.com/opencart-rest-api-documentations/?utm=simple_shopping\"
                                target=\"_blank\" class=\"alert-link\">Documentation</a>
                        - You will find walkthrough <a href=\"https://opencart-api.com/tutorial/?utm=simple_shopping\"
                                                       target=\"_blank\" class=\"alert-link\">videos</a>,
                        <a href=\"https://opencart-api.com/faqs/?utm=simple_shopping\" target=\"_blank\" class=\"alert-link\">FAQs</a>,
                        <a href=\"https://opencart-api.com/forum/?utm=simple_shopping\" target=\"_blank\"
                           class=\"alert-link\">forum</a> and more.
                    </p>
                    <p>
                        You can find working PHP demo scripts <a
                                href=\"https://opencart-api.com/opencart-rest-api-demo-clients/?utm=simple_shopping\"
                                target=\"_blank\" class=\"alert-link\">here</a>.
                    </p>
                    <p>
                        If you have any questions about the extension, please do not hesitate to <a
                                href=\"https://opencart-api.com/contact/?utm=simple_shopping\" target=\"_blank\"
                                class=\"alert-link\">contact us</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script type=\"text/javascript\"><!--
    \$('#button-generate').on('click', function() {
        rand = '';

        string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for (i = 0; i < 32; i++) {
            rand += string[Math.floor(Math.random() * (string.length - 1))];
        }

        \$('#input-key').val(rand);
    });
    //--></script>
";
        // line 158
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/shopping_cart_rest_api.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  307 => 158,  262 => 115,  253 => 109,  247 => 106,  242 => 105,  240 => 104,  237 => 103,  232 => 101,  227 => 99,  223 => 98,  213 => 91,  205 => 85,  203 => 84,  196 => 80,  192 => 79,  184 => 74,  179 => 72,  171 => 67,  167 => 66,  159 => 61,  151 => 56,  147 => 55,  140 => 50,  135 => 48,  130 => 47,  125 => 45,  120 => 44,  118 => 43,  112 => 40,  105 => 37,  99 => 35,  97 => 34,  91 => 31,  87 => 29,  79 => 25,  76 => 24,  68 => 20,  66 => 19,  60 => 15,  49 => 13,  45 => 12,  40 => 10,  33 => 8,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*     <div class="page-header">*/
/*         <div class="container-fluid">*/
/*             <div class="pull-right">*/
/*                 <button type="submit" form="form-module" data-toggle="tooltip" title="{{ button_save }}"*/
/*                         class="save-changes btn btn-primary"><i class="fa fa-save"></i></button>*/
/*                 <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i*/
/*                             class="fa fa-reply"></i></a></div>*/
/*             <h1>{{ heading_title }}</h1>*/
/*             <ul class="breadcrumb">*/
/*                 {% for breadcrumb in breadcrumbs %}*/
/*                     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*                 {% endfor %}*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="container-fluid">*/
/*         {% if error_warning %}*/
/*             <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*             </div>*/
/*         {% endif %}*/
/*         {% if install_success %}*/
/*             <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> {{ install_success }}*/
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*             </div>*/
/*         {% endif %}*/
/*         <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*                 <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                 {% if module_shopping_cart_rest_api_licensed_on is empty %}*/
/*                     {{ xcDLOMddkCV }}*/
/*                 {% endif %}*/
/*                 <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-module"*/
/*                       class="form-horizontal">*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*                         <div class="col-sm-10">*/
/*                             <select name="module_shopping_cart_rest_api_status" id="input-status" class="form-control restapi_status">*/
/*                                 {% if module_shopping_cart_rest_api_status %}*/
/*                                     <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                                     <option value="0">{{ text_disabled }}</option>*/
/*                                 {% else %}*/
/*                                     <option value="1">{{ text_enabled }}</option>*/
/*                                     <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                                 {% endif %}*/
/*                             </select>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-2 control-label" for="input-entry-allowed-ip">*/
/*                     <span data-toggle="tooltip" title="" data-original-title="{{ text_allowed_ip }}">*/
/*                       {{ entry_allowed_ip }}*/
/*                     </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input class="form-control" type="text" name="module_shopping_cart_rest_api_allowed_ip"*/
/*                                    value="{{ module_shopping_cart_rest_api_allowed_ip }}"/>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-2 control-label" for="input-entry-key">*/
/*               <span data-toggle="tooltip" title="" data-original-title="{{ text_secret_key }}">*/
/*                 {{ entry_key }}*/
/*               </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input class="form-control" id="input-key" type="text" name="module_shopping_cart_rest_api_key"*/
/*                                    value="{{ module_shopping_cart_rest_api_key }}" required="required"/>*/
/*                             <br>*/
/*                             <button type="button" id="button-generate" class="btn btn-primary"><i class="fa fa-refresh"></i> {{ button_generate_api_key }}</button>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-2 control-label" for="input-entry-order-id">*/
/*                   <span data-toggle="tooltip" title="" data-original-title="{{ text_order_id }}">*/
/*                     {{ entry_order_id }}*/
/*                   </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             {% if module_shopping_cart_rest_api_licensed_on is empty %}*/
/*                                 <div class="restAPILicenseError"></div>*/
/*                                 <div class="restAPILicenseSuccess"></div>*/
/*                                 <div class="restAPILicenseInfo"></div>*/
/*                                 <table class="table">*/
/*                                     <tr>*/
/*                                         <td colspan="2" style="border: none;">*/
/*                                             <input type="text" class="rest_api_ord_id form-control" placeholder="REST-XXX-XXX" name="module_shopping_cart_rest_api_order_id" id="module_shopping_cart_rest_api_order_id" value="{{ module_shopping_cart_rest_api_order_id }}" required="required" />*/
/*                                             <br>*/
/*                                             <button type="button" class="btn btn-success activateLicenseButton"><i class="icon-ok"></i> Activate License</button>*/
/*                                         </td>*/
/*                                     </tr>*/
/*                                 </table>*/
/*                                 <script type="text/javascript">*/
/*                                     var domain='{{ hostname }}';*/
/*                                     var timenow={{ 'now'|date('U') }};*/
/*                                 </script>*/
/*                                 <script type="text/javascript" src="//license.opencart-api.com/validate.js?v={{ 'now'|date('U') }}"></script>*/
/*                             {% endif %}*/
/* */
/*                             {% if module_shopping_cart_rest_api_licensed_on is not empty %}*/
/*                                 <input type="hidden" class="rest_api_ord_id form-control" name="module_shopping_cart_rest_api_order_id" id="module_shopping_cart_rest_api_order_id" value="{{ module_shopping_cart_rest_api_order_id }}" required="required" />*/
/*                                 <input name="nJvNVJoMHcQVIuHk" type="hidden" value="{{ module_shopping_cart_rest_api_licensed_on }}" />*/
/*                                 <table class="table licensedTable">*/
/*                                     <tr>*/
/*                                         <td style="border:none;"><b>{{ module_shopping_cart_rest_api_order_id }}</b>*/
/*                                             <span style="text-align:center;background-color:#c0f7a5;display: inline-block;padding: 5px 10px;">VALID LICENSE</span>*/
/*                                         </td>*/
/*                                     </tr>*/
/*                                 </table>*/
/*                             {% endif %}*/
/*                         </div>*/
/*                     </div>*/
/*                 </form>*/
/*                 <div class="alert alert-info">*/
/*                     <h4><i class="fa fa-info"></i> Info</h4>*/
/*                     <p>Please follow the instructions in install.txt to install REST API extension.</p>*/
/*                     <p>If you need help please check out our <a*/
/*                                 href="https://opencart-api.com/opencart-rest-api-documentations/?utm=simple_shopping"*/
/*                                 target="_blank" class="alert-link">Documentation</a>*/
/*                         - You will find walkthrough <a href="https://opencart-api.com/tutorial/?utm=simple_shopping"*/
/*                                                        target="_blank" class="alert-link">videos</a>,*/
/*                         <a href="https://opencart-api.com/faqs/?utm=simple_shopping" target="_blank" class="alert-link">FAQs</a>,*/
/*                         <a href="https://opencart-api.com/forum/?utm=simple_shopping" target="_blank"*/
/*                            class="alert-link">forum</a> and more.*/
/*                     </p>*/
/*                     <p>*/
/*                         You can find working PHP demo scripts <a*/
/*                                 href="https://opencart-api.com/opencart-rest-api-demo-clients/?utm=simple_shopping"*/
/*                                 target="_blank" class="alert-link">here</a>.*/
/*                     </p>*/
/*                     <p>*/
/*                         If you have any questions about the extension, please do not hesitate to <a*/
/*                                 href="https://opencart-api.com/contact/?utm=simple_shopping" target="_blank"*/
/*                                 class="alert-link">contact us</a>.*/
/*                     </p>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <script type="text/javascript"><!--*/
/*     $('#button-generate').on('click', function() {*/
/*         rand = '';*/
/* */
/*         string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';*/
/* */
/*         for (i = 0; i < 32; i++) {*/
/*             rand += string[Math.floor(Math.random() * (string.length - 1))];*/
/*         }*/
/* */
/*         $('#input-key').val(rand);*/
/*     });*/
/*     //--></script>*/
/* {{ footer }}*/
/* */
