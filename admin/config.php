<?php
// HTTP
define('HTTP_SERVER', 'http://omnichannel.com.vn/admin/');
define('HTTP_CATALOG', 'http://omnichannel.com.vn/');

// HTTPS
define('HTTPS_SERVER', 'http://omnichannel.com.vn/admin/');
define('HTTPS_CATALOG', 'http://omnichannel.com.vn/');

// DIR
define('DIR_APPLICATION', '/var/www/source/dev/DEMO_THEME/f-1005/admin/');
define('DIR_SYSTEM', '/var/www/source/dev/DEMO_THEME/f-1005/system/');
define('DIR_IMAGE', '/var/www/source/dev/DEMO_THEME/f-1005/image/');
define('DIR_STORAGE', '/var/www/source/dev/DEMO_THEME/f-1005/storage/');
define('DIR_CATALOG', '/var/www/source/dev/DEMO_THEME/f-1005/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'Kpis@123');
define('DB_DATABASE', 'kpis_f-1005');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
